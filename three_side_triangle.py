# -*- coding: utf-8 -*-

import re
import Rhino
import Rhino.Geometry as rg
import rhinoscriptsyntax as rs
from scriptcontext import doc, sticky
import System.Drawing.Color as syscolor


def DrawFixedSegmentLengthPolyLine(pds, args, crvs_ref):
    if len(pds) < 2: return
    vec1 = rg.Vector3d(pds[-2][0])
    vec2 = rg.Vector3d(pds[-1][0])
    vec = vec1 - vec2
    vec.Unitize()
    if (len(pds) > 3): pds[-2][0] = rg.Point3d(pds[-1][0] + vec * pds[-1][1])
    else: pds[-1][0] = rg.Point3d(pds[-2][0] - vec * pds[-1][1])
    curve = rg.LineCurve(pds[-1][0], pds[-2][0])
    if (args): 
        args.Display.DrawCurve(curve.ToNurbsCurve(), syscolor.HotPink, 3)
        mid_point = curve.PointAt(curve.Domain.Mid)
        #args.Display.DrawPoint(mid_point, 0, 30, syscolor.Aqua)
        text = "%.2f" % pds[-1][1]
        #args.Display.Draw2dText(text, syscolor.Black, mid_point, True)
        text3d = Rhino.Display.Text3d(text)
        text3d.Height = 0.3
        text3d.Bold = False
        args.Display.Draw3dText(text3d, syscolor.DimGray, mid_point)
        #args.Display.DrawDot(mid_point, text, syscolor.Aqua, syscolor.Black)
    else: crvs_ref.append(curve.ToNurbsCurve())
    DrawFixedSegmentLengthPolyLine(pds[:-1], args, crvs_ref)

def GetNextPoint(gp, dblOption_Length):
    while gp.Get()!= Rhino.Input.GetResult.Cancel:
        if gp.CommandResult() != Rhino.Commands.Result.Success:
            return None
        if gp.Result() != Rhino.Input.GetResult.Option:
            point = sticky['Point']
            gp.Tag.append([point, sticky['Length']])
            break
        if gp.Result() == Rhino.Input.GetResult.Option:
            sticky['Length'] = dblOption_Length.CurrentValue

def LineToArc(line, center):
    start = line.PointAtStart
    end = line.PointAtEnd
    mid = line.PointAt(line.Domain.Mid)
    vec = rg.Vector3d(mid - center)
    vec.Unitize()
    mid = mid + vec*0.2
    arc = rg.Arc(start, mid, end).ToNurbsCurve()
    return arc

def DrawTriangle(point):
    def dynamic_draw_curve(sender, args):
        args.Source.Tag.append([args.CurrentPoint, sticky['Length']])
        DrawFixedSegmentLengthPolyLine(args.Source.Tag, args, [])
        sticky['Point'] = args.Source.Tag[-1][0]
        args.Source.Tag.pop()
    result = None
    junk = []
    anchor = rs.AddPoint(point)
    junk.append(anchor)
    if sticky.has_key('Length'):
        Length = sticky['Length']
    else:
        Length = 10
        sticky['Length'] = Length
    sticky['Point'] = point

    gp = Rhino.Input.Custom.GetPoint()
    dblOption_Length = Rhino.Input.Custom.OptionDouble(Length)
    gp.Tag = [[point, 0]]
    gp.AddOptionDouble("Length", dblOption_Length)
    gp.AcceptUndo(False)
    gp.EnableTransparentCommands(True)
    gp.SetCommandPrompt("Set second point")
    gp.DynamicDraw += dynamic_draw_curve
    for i in range(3):
        GetNextPoint(gp, dblOption_Length)
    crvs = []
    DrawFixedSegmentLengthPolyLine(gp.Tag, None, crvs)
    crv_ids = []
    for crv in crvs: crv_ids.append(doc.Objects.AddCurve(crv))
    if (len(crv_ids) > 1): 
        join = rs.JoinCurves(crv_ids, True)[0]
        junk.append(join)
        cen = rs.CurveAreaCentroid(join)[0]
        arc_ids = []
        for crv in crvs: arc_ids.append(doc.Objects.AddCurve(LineToArc(crv, cen)))
        cont = rs.JoinCurves(arc_ids, True)[0]
        junk.append(cont)
        result = rs.AddPlanarSrf(cont)
    rs.DeleteObjects(junk)
    return result


if __name__ == "__main__":
    gp = Rhino.Input.Custom.GetPoint()
    gp.SetCommandPrompt("Set first point")
    point = None
    while gp.Get()!= Rhino.Input.GetResult.Cancel:
        point = gp.Point()
        break
    if point:
        DrawTriangle(point)
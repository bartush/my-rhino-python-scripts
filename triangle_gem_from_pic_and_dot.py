# -*- coding: utf-8 -*-

import re
import Rhino
import Rhino.Geometry as rg
import rhinoscriptsyntax as rs
from scriptcontext import doc, sticky

def select_pics_n_dots():
    pics_n_dots = []
    go = Rhino.Input.Custom.GetObject()
    go.SetCommandPrompt("Select pics & dots")
    go.GeometryFilter = Rhino.DocObjects.ObjectType.Surface | Rhino.DocObjects.ObjectType.TextDot
    go.GroupSelect = True;
    go.SubObjectSelect = False
    go.GetMultiple(1,0) # minmum number = 1, maximum number = 0, 0 - means multiple
    result = go.CommandResult()
    rs.UnselectAllObjects()
    if result == Rhino.Commands.Result.Success:
        objects = go.Objects()
        for obj1, obj2 in zip(objects[0::2], objects[1::2]):
            if obj1.Brep(): p = obj1.ObjectId
            if obj1.TextDot(): d = obj1.TextDot()
            if obj2.Brep(): p = obj2.ObjectId
            if obj2.TextDot(): d = obj2.TextDot()
            if p and d: pics_n_dots.append((p,d))
    return pics_n_dots

def new_group_name(name):
    count = 0
    existing_group_names = GroupNames()
    while True:
        new_group_name = name + str(count)
        if existing_group_names and new_group_name in existing_group_names:
            count += 1
        else:
            break
    return new_group_name

def create_gem(brep_id, h):
    top_coef = 0.2
    rundist_coef = 0.1
    pavilion_coef = 0.7
    
    rs.EnableRedraw(False)
    surfaces = []
    junk = []
    brep = Rhino.DocObjects.ObjRef(brep_id).Brep()
    border = rs.DuplicateSurfaceBorder(brep)[0]
    junk.append(border)
    centroid_point = rs.SurfaceAreaCentroid(brep_id)[0]
    
    gem_top_point = rg.Point3d(centroid_point)
    gem_top_point.Z += h * (top_coef + rundist_coef)
    
    rundist_top_point = rg.Point3d(centroid_point)
    rundist_top_point.Z += h * rundist_coef
    
    pavilion_bottom_point = rg.Point3d(centroid_point)
    pavilion_bottom_point.Z -= h * pavilion_coef
    
    rundist = rs.ExtrudeCurveStraight(border, centroid_point, rundist_top_point)
    if rundist: surfaces.append(rundist)
    
    pavilion = rs.ExtrudeCurvePoint(border, pavilion_bottom_point)
    if pavilion: surfaces.append(pavilion)
    
    border = rs.MoveObject(border, [0, 0, h * rundist_coef])
    top_border = rs.CopyObject(border, [0, 0, h * top_coef])
    if top_border: junk.append(top_border)
    rs.ScaleObject(top_border, rs.CurveAreaCentroid(top_border)[0], [0.7, 0.7, 0.7])
    
    top_surf = rs.AddLoftSrf([border, top_border])
    if top_surf: surfaces.append(top_surf)
    
    cap = rs.AddPlanarSrf(top_border)
    if cap: surfaces.append(cap)
    
    result = rs.JoinSurfaces(surfaces, True)
    
    rs.DeleteObjects(junk)
    rs.EnableRedraw(True)
    return result

if __name__ == "__main__":
    pnds = select_pics_n_dots()
    for pnd in pnds:
        dot_text = pnd[1].Text
        pic_id = pnd[0]
        #re_search = re.search('^(.*)#(.*)-(.*)', dot_text)
        #dim = float(re_search.group(2))
        re_search = re.search('(-)(.*)', dot_text)
        height = float(re_search.group(2))
        create_gem(pic_id, height)
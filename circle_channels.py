import Rhino
import math
#from rhinoscriptsyntax import UnselectAllObjects, AddLoftSrf, OffsetSurface, DeleteObjects, ReverseCurve, ExplodeCurves, ExplodePolysurfaces, AddGroup, AddObjectsToGroup, GroupNames, SurfaceDegree, RebuildSurface, SurfacePointCount, EnableRedraw, DuplicateSurfaceBorder, FlipSurface
#from scriptcontext import doc, sticky
import rhinoscriptsyntax as rs

def draw_box(center, width):
    plane = rs.MovePlane(rs.WorldXYPlane(),
        [center[0] - width/2, center[1] - width/2, 0])
    return rs.AddRectangle(plane, width, width)


def channel_box_params(xl, xr, r):
    """
    xl an xr are left and right boundaries
    both positive such that xr > xl
    r is raduis of circle
    """
    yl = math.sqrt(r*r-xl*xl)
    yr = math.sqrt(r*r-xr*xr)
    RR = math.sqrt(xr*xr + yl*yl)
    alpha = abs(math.pi.real*0.25 - math.asin(xr/RR))
    sin_gamma = RR * math.sin(alpha)/r
    gamma = abs(math.pi.real - math.asin(sin_gamma))
    beta = math.pi.real - alpha - gamma
    d = math.sqrt(r*r + RR*RR - 2*r*RR*math.cos(beta))
    w = 0.5 * math.sqrt(2) * d
    xc = xr - 0.5 * w
    yc = yl - 0.5 * w
    return [[xc,yc], w]

def draw_symetric_boxes(center, width):
        draw_box(center, width)
        if (abs(center[0] - center[1]) > 1e-2):
            draw_box([center[1], center[0], 0], width)
            
        draw_box([-center[0], center[1], 0], width)
        if (abs(center[0] - center[1]) > 1e-2):
            draw_box([center[1], -center[0], 0], width)

        draw_box([center[0], -center[1], 0], width)
        if (abs(center[0] - center[1]) > 1e-2):
            draw_box([-center[1], center[0], 0], width)

        draw_box([-center[0], -center[1], 0], width)
        if (abs(center[0] - center[1]) > 1e-2):
            draw_box([-center[1], -center[0], 0], width)

def test1():
    n = 10
    for i in range(0, n):
        xl = i * r / n;
        xr = (i + 1) * r / n;
        [[xc, yc,],w] = channel_box_params(xl, xr, r)
        draw_box([xc, yc, 0], w)
        [[xc, yc,],w] = channel_box_params(xl, xr, r)
        draw_box([xc, -yc, 0], w)
        [[xc, yc,],w] = channel_box_params(xl, xr, r)
        draw_box([-xc, -yc, 0], w)
        [[xc, yc,],w] = channel_box_params(xl, xr, r)
        draw_box([-xc, yc, 0], w)

def test2():
    w = 10
    xl = 0
    xr = r
    while w > 0.3:
        [[xc, yc,],w] = channel_box_params(xl, xr, r)
        draw_symetric_boxes([xc, yc, 0], w)
        xr = xc - 0.5 * w

if __name__ == "__main__":
    r = 5
    #rs.AddCircle(rs.WorldXYPlane(), r)
    draw_box([0, 0, 0], r * 2)
    #test1()
    test2()
    

